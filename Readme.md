# Shell powerhouse

Docker image builder to allow heavy debugging in containerized environment.

## Content

The image contains the following CLI utilities:

- aws
- bash
- curl
- dig
- gcloud
- git
- host
- jq
- kubectl
- mysql
- netcat / nc
- nmap
- nslookup
- openssl
- psql
- ssh
- vim

## Usage

### Out of the box
- Pull the existing image : `docker pull fricounet/shell-powerhouse`
- Run the container : `docker run -it shell-powerhouse`
- Enjoy !

### Build your own
- Go the repository : `cd shell-powerhouse`
- Build the image : `docker build . -t shell-powerhouse`
- Run the container : `docker run -it shell-powerhouse`
- Enjoy !

## Releases

- 1.0.0 - Initial release with the following utilities:
    - aws
    - bash
    - curl
    - dig
    - gcloud
    - git
    - host
    - jq
    - kubectl
    - mysql
    - netcat / nc
    - nmap
    - nslookup
    - openssl
    - psql
    - ssh
    - vim
