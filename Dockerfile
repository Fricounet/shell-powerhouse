FROM alpine:3.13.5

LABEL maintainer="Baptiste Girard-Carrabin <baptiste.gc@outlook.fr>"

## Install basic utilities
RUN apk add --update --no-cache \
    bash \
    bash-completion \
    bind-tools \
    ca-certificates \
    curl \
    git \
    jq \
    mariadb-client \
    netcat-openbsd \
    nmap \
    openssh-client \
    postgresql-client \
    py3-pip \
    python3 \
    vim \
    && rm -rf /var/cache/apk/*

## Install AWS CLI
RUN pip3 install awscli


## Install GCLOUD CLI
RUN curl -sSL https://sdk.cloud.google.com | bash
ENV PATH $PATH:/root/google-cloud-sdk/bin


## Install Kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl \
    && chmod +x kubectl \
    && mv ./kubectl /usr/bin/kubectl

## Add python alias for python3
RUN if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi

COPY bin/startup.sh .
RUN "./startup.sh"

CMD ["/bin/bash"]
ENTRYPOINT [ "tail", "-f", "/dev/null" ]
